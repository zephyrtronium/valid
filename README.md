# valid

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/zephyrtronium/valid.svg)](https://pkg.go.dev/gitlab.com/zephyrtronium/valid)

Package valid implements explicit, efficient, type-safe validation.

```go
productDB := map[string]bool{
	"Gibson Les Paul Custom": true,
	"Yamaha Pacifica":        true,
}

inv := Invoice{
    Customer: "",
    Lines: []LineItem{
        {"Gibson Les Paul Custom", 2000},
        {"Gibson Les Paul Jr", 1500},
        {"Yamaha Pacifica", -1200},
    },
}

err := valid.Check(nil, []valid.Condition{
    {Name: "customer name", Missing: inv.Customer == ""},
    {Name: "customer name (too long)", Invalid: len(inv.Customer) > 50},
    {Name: "line items", Missing: len(inv.Lines) == 0},
})
for i, l := range inv.Lines {
    err = valid.Category(err, "line item", i+1, []valid.Condition{
        {Name: "product", Missing: l.Product == "", Invalid: !productDB[l.Product]},
        {Name: "amount", Missing: l.Amount == 0, Invalid: l.Amount < 0},
    })
}

fmt.Println(err)
```

```
Output:
missing customer name, invalid line item 2 product, invalid line item 3 amount
```


## Why?

The most popular validation packages rely on a complex library of options placed in struct tags.
This comes with a number of issues:
- The complete list of options is "somewhere" in documentation, with no guarantee that it hasn't drifted from the implementation.
- It is inevitable that some validation functionality is missing.
- Only struct fields can be validated by the principal mechanism.
- Struct tags are inherently not type safe.
  They are plain strings, so it is possible to misspell an option or to apply an option to a field for which it makes no sense.
  These problems are only detected when the validation code runs (which your coworker will ensure happens first in production).

Package valid instead provides validation in the form of code.
To validate a form, you construct a `[]valid.Condition` enumerating each field's conditions.
A field may be missing or invalid, each of which is checked as a `bool`.

Writing a nonsense validation condition causes a type error, preventing the code from compiling.
The "available options" are whatever predicates you write.
They can never drift from the implementation, because they *are* the implementation.

Compared to heavyweight reflection and complicated tag parsing, package valid's validation is close to optimal.
The only extraneous work is creating a slice of small structs.
The validation functions are also designed (and tested) to have zero allocations when validation succeeds.


## License

Copyright 2023 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
