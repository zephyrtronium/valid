/*
Copyright 2023 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package valid implements explicit, efficient, type-safe validation.
//
// The main mechanism to validate data is to pass a list of [Condition]
// values to [Check]. Since each condition is a struct containing a name
// (only used for validation failure messages) and bools, validation is
// type-safe: it isn't possible to apply a condition which is nonsense by the
// type system, or else the program will fail to compile.
//
// Validation is allocation-free except on failure. To facilitate this,
// subcategories (e.g. line items on an invoice, revisions in a Git repo, ...)
// can be additionally validated using [Category], which is carefully designed
// to inline category and entry names in validation messages without
// allocations on valid attributes.
//
// The validation functions take in errors as input. If the error is the result
// of a previous call to Check or Category, then any additional failed
// validations are appended to that error. Otherwise, the returned error wraps
// the input on failure. This tremendously simplifies composition of validation
// errors, as categories can be accumulated in a loop, and any original error
// can be checked using [errors.Is] or [errors.As].
package valid

import (
	"fmt"
	"strings"
)

// Condition is a validation condition.
type Condition struct {
	// Name is the field validated by this condition.
	// It is used only in error messages.
	Name string
	// Missing indicates that a required field is missing.
	Missing bool
	// Invalid indicates that a provided field has an invalid value.
	// If a field is both missing and invalid, the resulting error string
	// describes it only as missing.
	Invalid bool
}

// Check runs validation. If any condition failed, the result is an error of
// type *[Error]. If err is also of type *[Error], then failed conditions
// are appended to its list. Otherwise, the result wraps err.
//
// If all validation conditions succeed, the result is err, and Check does
// not allocate.
func Check(err error, conds []Condition) error {
	var r *Error
	for i, c := range conds {
		if !c.Missing && !c.Invalid {
			continue
		}
		if r == nil {
			// Check if err is already a validation result. If it is, we can
			// add to it. Otherwise we make a new validation error wrapping it
			// (even if it's currently nil).
			if r, _ = err.(*Error); r == nil {
				r = &Error{
					Failed:  make([]Condition, 0, len(conds)-i),
					wrapped: err,
				}
			}
			err = r
		}
		r.Failed = append(r.Failed, c)
	}
	return err
}

// Category performs additional validation with failed conditions recorded
// under a category name. It works like [Check] except that field names are
// augmented with the category and entry when a condition fails. The entry
// is formatted as if by [fmt.Sprint].
func Category[Cat any](err error, cat string, entry Cat, conds []Condition) error {
	var r *Error
	for i, c := range conds {
		if !c.Missing && !c.Invalid {
			continue
		}
		if r == nil {
			if r, _ = err.(*Error); r == nil {
				r = &Error{
					Failed:  make([]Condition, 0, len(conds)-i),
					wrapped: err,
				}
			}
			err = r
		}
		c.Name = fmt.Sprintf("%s %v %s", cat, entry, c.Name)
		r.Failed = append(r.Failed, c)
	}
	return err
}

// Error is a validation error.
type Error struct {
	// Failed is the list of validation conditions which failed.
	Failed []Condition
	// wrapped is a wrapped error.
	wrapped error
}

// Error returns the failed validation conditions separated by commas.
// If a field is both missing and invalid, it is described only as missing.
func (err *Error) Error() string {
	var b strings.Builder
	// Add the first field separately so we can add separators unconditionally.
	c := err.Failed[0]
	if c.Missing {
		b.WriteString("missing ")
	} else {
		b.WriteString("invalid ")
	}
	b.WriteString(c.Name)
	// Now add each remaining field.
	for _, c := range err.Failed[1:] {
		if c.Missing {
			b.WriteString(", missing ")
		} else {
			b.WriteString(", invalid ")
		}
		b.WriteString(c.Name)
	}

	if err.wrapped != nil {
		b.WriteString("; and ")
		b.WriteString(err.wrapped.Error())
	}
	return b.String()
}

// Unwrap returns any wrapped error.
func (err *Error) Unwrap() error {
	return err.wrapped
}

// Not takes an ignored value of any type and an error and reports whether
// the error is non-nil. At the cost of potential redundant work, it can
// simplify reporting invalid values by applying a parse function.
func Not[Ignored any](_ Ignored, err error) bool {
	return err != nil
}
