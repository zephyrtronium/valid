package valid_test

import (
	"fmt"

	"gitlab.com/zephyrtronium/valid"
)

type Invoice struct {
	Customer string
	Lines    []LineItem
}

type LineItem struct {
	Product string
	Amount  float64
}

var ProductDB = map[string]bool{
	"Gibson Les Paul Custom": true,
	"Yamaha Pacifica":        true,
}

func Example() {
	inv := Invoice{
		Customer: "",
		Lines: []LineItem{
			{"Gibson Les Paul Custom", 2000},
			{"Gibson Les Paul Jr", 1500},
			{"Yamaha Pacifica", -1200},
		},
	}

	err := valid.Check(nil, []valid.Condition{
		{Name: "customer name", Missing: inv.Customer == ""},
		{Name: "customer name (too long)", Invalid: len(inv.Customer) > 50},
		{Name: "line items", Missing: len(inv.Lines) == 0},
	})
	for i, l := range inv.Lines {
		err = valid.Category(err, "line item", i+1, []valid.Condition{
			{Name: "product", Missing: l.Product == "", Invalid: !ProductDB[l.Product]},
			{Name: "amount", Missing: l.Amount == 0, Invalid: l.Amount < 0},
		})
	}

	fmt.Println(err)
	// Output:
	// missing customer name, invalid line item 2 product, invalid line item 3 amount
}
