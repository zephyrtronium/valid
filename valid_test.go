/*
Copyright 2023 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package valid_test

import (
	"errors"
	"fmt"
	"strconv"
	"testing"

	"gitlab.com/zephyrtronium/valid"
)

var errWrapped = errors.New("kessoku band")

func TestCheck(t *testing.T) {
	cases := []struct {
		name    string
		in      error
		conds   []valid.Condition
		want    string
		wrapped error
	}{
		{
			name:  "none",
			in:    nil,
			conds: nil,
			want:  "",
		},
		{
			name: "ok",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: false, Invalid: false},
				{Name: "ryou", Missing: false, Invalid: false},
			},
			want: "",
		},
		{
			name: "wrapped",
			in:   errWrapped,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: false, Invalid: false},
				{Name: "ryou", Missing: false, Invalid: false},
			},
			want: errWrapped.Error(),
		},
		{
			name: "one-missing",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: false},
				{Name: "ryou", Missing: false, Invalid: false},
			},
			want: "missing bocchi",
		},
		{
			name: "four-missing",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: false},
				{Name: "ryou", Missing: true, Invalid: false},
				{Name: "nijika", Missing: true, Invalid: false},
				{Name: "kita", Missing: true, Invalid: false},
			},
			want: "missing bocchi, missing ryou, missing nijika, missing kita",
		},
		{
			name: "one-invalid",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: false, Invalid: true},
				{Name: "ryou", Missing: false, Invalid: false},
			},
			want: "invalid bocchi",
		},
		{
			name: "four-invalid",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: false, Invalid: true},
				{Name: "ryou", Missing: false, Invalid: true},
				{Name: "nijika", Missing: false, Invalid: true},
				{Name: "kita", Missing: false, Invalid: true},
			},
			want: "invalid bocchi, invalid ryou, invalid nijika, invalid kita",
		},
		{
			name: "one-missing-invalid",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: true},
				{Name: "ryou", Missing: false, Invalid: false},
			},
			want: "missing bocchi",
		},
		{
			name: "four-missing-invalid",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: true},
				{Name: "ryou", Missing: true, Invalid: true},
				{Name: "nijika", Missing: true, Invalid: true},
				{Name: "kita", Missing: true, Invalid: true},
			},
			want: "missing bocchi, missing ryou, missing nijika, missing kita",
		},
		{
			name: "append",
			in: valid.Check(nil, []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: true},
			}),
			conds: []valid.Condition{
				{Name: "ryou", Missing: false, Invalid: true},
			},
			want: "missing bocchi, invalid ryou",
		},
		{
			name: "append-wrapped",
			in: valid.Check(errWrapped, []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: true},
			}),
			conds: []valid.Condition{
				{Name: "ryou", Missing: false, Invalid: true},
			},
			want:    "missing bocchi, invalid ryou; and " + errWrapped.Error(),
			wrapped: errWrapped,
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			err := valid.Check(c.in, c.conds)
			if err == nil {
				if c.want == "" {
					// As expected.
					return
				}
				t.Fatalf("expected error %q but got nil", c.want)
			}
			msg := err.Error()
			if msg != c.want {
				t.Errorf("wrong error message from %#v:\nwant %q\ngot  %q", err, c.want, msg)
			}
			w, _ := err.(interface{ Unwrap() error })
			if w == nil {
				if c.wrapped == nil {
					// As expected.
					return
				}
				t.Fatalf("expected wrapped error %q but result does not unwrap", c.wrapped)
			}
			if w.Unwrap() != c.wrapped {
				t.Errorf("wrong wrapped error:\nwant %q\ngot  %q", c.wrapped, w.Unwrap())
			}
		})
	}
}

func TestCategory(t *testing.T) {
	cases := []struct {
		name    string
		in      error
		conds   []valid.Condition
		want    string
		wrapped error
	}{
		{
			name:  "none",
			in:    nil,
			conds: nil,
			want:  "",
		},
		{
			name: "ok",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: false, Invalid: false},
				{Name: "ryou", Missing: false, Invalid: false},
			},
			want: "",
		},
		{
			name: "wrapped",
			in:   errWrapped,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: false, Invalid: false},
				{Name: "ryou", Missing: false, Invalid: false},
			},
			want: errWrapped.Error(),
		},
		{
			name: "one-missing",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: false},
				{Name: "ryou", Missing: false, Invalid: false},
			},
			want: "missing kessoku band member bocchi",
		},
		{
			name: "four-missing",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: false},
				{Name: "ryou", Missing: true, Invalid: false},
				{Name: "nijika", Missing: true, Invalid: false},
				{Name: "kita", Missing: true, Invalid: false},
			},
			want: "missing kessoku band member bocchi, missing kessoku band member ryou, missing kessoku band member nijika, missing kessoku band member kita",
		},
		{
			name: "one-invalid",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: false, Invalid: true},
				{Name: "ryou", Missing: false, Invalid: false},
			},
			want: "invalid kessoku band member bocchi",
		},
		{
			name: "four-invalid",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: false, Invalid: true},
				{Name: "ryou", Missing: false, Invalid: true},
				{Name: "nijika", Missing: false, Invalid: true},
				{Name: "kita", Missing: false, Invalid: true},
			},
			want: "invalid kessoku band member bocchi, invalid kessoku band member ryou, invalid kessoku band member nijika, invalid kessoku band member kita",
		},
		{
			name: "one-missing-invalid",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: true},
				{Name: "ryou", Missing: false, Invalid: false},
			},
			want: "missing kessoku band member bocchi",
		},
		{
			name: "four-missing-invalid",
			in:   nil,
			conds: []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: true},
				{Name: "ryou", Missing: true, Invalid: true},
				{Name: "nijika", Missing: true, Invalid: true},
				{Name: "kita", Missing: true, Invalid: true},
			},
			want: "missing kessoku band member bocchi, missing kessoku band member ryou, missing kessoku band member nijika, missing kessoku band member kita",
		},
		{
			name: "append",
			in: valid.Check(nil, []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: true},
			}),
			conds: []valid.Condition{
				{Name: "ryou", Missing: false, Invalid: true},
			},
			want: "missing bocchi, invalid kessoku band member ryou",
		},
		{
			name: "append-wrapped",
			in: valid.Check(errWrapped, []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: true},
			}),
			conds: []valid.Condition{
				{Name: "ryou", Missing: false, Invalid: true},
			},
			want:    "missing bocchi, invalid kessoku band member ryou; and " + errWrapped.Error(),
			wrapped: errWrapped,
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			err := valid.Category(c.in, "kessoku band", "member", c.conds)
			if err == nil {
				if c.want == "" {
					// As expected.
					return
				}
				t.Fatalf("expected error %q but got nil", c.want)
			}
			msg := err.Error()
			if msg != c.want {
				t.Errorf("wrong error message from %#v:\nwant %q\ngot  %q", err, c.want, msg)
			}
			w, _ := err.(interface{ Unwrap() error })
			if w == nil {
				if c.wrapped == nil {
					// As expected.
					return
				}
				t.Fatalf("expected wrapped error %q but result does not unwrap", c.wrapped)
			}
			if w.Unwrap() != c.wrapped {
				t.Errorf("wrong wrapped error:\nwant %q\ngot  %q", c.wrapped, w.Unwrap())
			}
		})
	}
}

func TestZeroAllocOnPass(t *testing.T) {
	cases := []struct {
		name string
		in   error
	}{
		{
			name: "nil",
			in:   nil,
		},
		{
			name: "wrap",
			in:   errWrapped,
		},
		{
			name: "append",
			in: valid.Check(nil, []valid.Condition{
				{Name: "bocchi", Missing: true, Invalid: true},
			}),
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			var err error
			allocs := testing.AllocsPerRun(1, func() {
				err = valid.Check(c.in, []valid.Condition{
					{Name: "ryou"},
					{Name: "nijika"},
					{Name: "kita"},
				})
				err = valid.Category(err, "guitar", "gibson", []valid.Condition{
					{Name: "les paul custom"},
					{Name: "les paul jr"},
				})
			})
			// All validation passed, so the error should be identical to the input.
			if err != c.in {
				t.Errorf("error changed:\n       %#v\nbecame %#v", c.in, err)
			}
			if allocs != 0 {
				t.Errorf("not zero allocs: %f", allocs)
			}
		})
	}
}

func TestNot(t *testing.T) {
	if valid.Not(0, nil) {
		t.Errorf("nil error called invalid")
	}
	if !valid.Not(0, errWrapped) {
		t.Errorf("non-nil error called valid")
	}
}

func ExampleCheck() {
	err := valid.Check(nil, []valid.Condition{
		{Name: "bocchi", Missing: false, Invalid: false},
		{Name: "ryou", Missing: true, Invalid: false},
		{Name: "nijika", Missing: false, Invalid: true},
		{Name: "kita", Missing: true, Invalid: true},
	})
	fmt.Println(err)
	// Output:
	// missing ryou, invalid nijika, missing kita
}

func ExampleCheck_wrapped() {
	wrapped := errors.New("anime isn't real")
	err := valid.Check(wrapped, []valid.Condition{
		{Name: "bocchi", Missing: false, Invalid: false},
		{Name: "ryou", Missing: true, Invalid: false},
		{Name: "nijika", Missing: false, Invalid: true},
		{Name: "kita", Missing: true, Invalid: true},
	})
	fmt.Println(err)
	fmt.Println(errors.Is(err, wrapped))
	// Output:
	// missing ryou, invalid nijika, missing kita; and anime isn't real
	// true
}

func ExampleCategory() {
	var err error
	for _, g := range []string{"Les Paul Custom", "Les Paul Jr"} {
		err = valid.Category(err, "Gibson", g, []valid.Condition{
			{Name: "strings", Missing: true},
		})
	}
	err = valid.Category(err, "Yamaha", "Pacifica", []valid.Condition{
		{Name: "strings", Invalid: true},
	})
	fmt.Println(err)
	// Output:
	// missing Gibson Les Paul Custom strings, missing Gibson Les Paul Jr strings, invalid Yamaha Pacifica strings
}

func ExampleNot() {
	fmt.Println(valid.Not(strconv.ParseInt("1", 10, 64)))
	fmt.Println(valid.Not(strconv.ParseInt("true", 10, 64)))
	// Output:
	// false
	// true
}
